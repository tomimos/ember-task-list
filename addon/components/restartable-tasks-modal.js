import Component from '@ember/component';
import { computed, observer } from '@ember/object';
import { A } from '@ember/array';

export default Component.extend({
  expanded: false,
  countOffset: 0,

  numberOfTasks: computed('tasks', 'tasks.[]', function() {
    let count = this.get('tasks.length') - this.get('countOffset');

    if (count < 0) {
      count = 0;
    }

    return count;
  }),

  numberOfCompletedTasks: computed('tasks', 'tasks.@each.complete', function() {
    let count = 0;
    let tasks = this.get('tasks');

    if (tasks) {
      tasks = A(tasks.filterBy('complete', true));

      count = tasks.length - this.get('countOffset');

      if (count < 0) {
        count = 0;
      }
    }

    return count;
  }),

  cancelDisabled: computed('mainTask.isRunning', 'retryTask.isRunning', function() {
    let isRunning = (this.get('mainTask') && this.get('mainTask.isRunning')) ||
                    (this.get('retryTask') && this.get('retryTask.isRunning'));

    return isRunning;
  }),

  retryDisabled: computed('mainTask.isRunning', 'retryTask.isRunning', 'tasks.@each.complete', function() {
    let incomplete = this.get('tasks') ? this.get('tasks').filterBy('complete', false) : null;
    let isRunning = (this.get('mainTask') && this.get('mainTask.isRunning')) ||
                    (this.get('retryTask') && this.get('retryTask.isRunning'));

    if (!Array.isArray(incomplete)) {
      return isRunning;
    }

    return incomplete.length === 0 || isRunning;
  }),

  failureObserver: observer('mainTask.isRunning', 'retryTask.isRunning', function() {
    if (this.get('mainTask.isRunning') || this.get('retryTask.isRunning')) {
      return false;
    }

    if (this.get('numberOfCompletedTasks') !== this.get('numberOfTasks')) {
      this.set('expanded', true);
    }
  }),

  actions: {
    retry() {
      this.set('expanded', false);
      this.retry().then(() => {
        let tasks = this.get('tasks');
        tasks = A(tasks.filterBy('complete', false));
        tasks = A(tasks.filterBy('cancelled', false));

        if (tasks.length === 0) {
          this.close();
        }
      });
    },

    cancel(index) {
      let tasks = this.get('tasks');

      if (Array.isArray(tasks)) {
        tasks = A(tasks.filterBy('complete', false));
        tasks = A(tasks.filterBy('cancelled', false));
      }

      if (tasks.length === 1) {
        this.cancelAll();
      } else {
        this.cancel(index);
      }
    },

    cancelAll() {
      this.cancelAll();
    },

    close() {
      this.close();
    },

    toggleExpanded() {
      this.set('expanded', !this.get('expanded'));
    }
  }
});
