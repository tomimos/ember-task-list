import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  classNames: ['ember-progress'],
  classNameBindings: ['size'],
  size: 'md',
  percentage: 0,

  width: computed('percentage', function() {
    return this.get('percentage') + '%';
  })
});
