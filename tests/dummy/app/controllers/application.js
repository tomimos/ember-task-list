import Controller from '@ember/controller';
import { A } from '@ember/array';
import { task, timeout } from 'ember-concurrency';
import EmberObject from '@ember/object';

export default Controller.extend({
  dummyTask: task(function * () {
    yield timeout(1000);
  }),

  processTasks: task(function * (tasks) {
    for (var i = 0; i < tasks.get('length'); i++) {
      let task = tasks.objectAt(i);

      yield task.task.perform();

      if (i === 3) {
        task.set('complete', false);
      } else {
        task.set('complete', true);
      }

      this.notifyPropertyChange('tasks');
    }
  }),

  reprocessTasks: task(function * (tasks) {
    for (var i = 0; i < tasks.get('length'); i++) {
      let task = tasks.objectAt(i);

      yield task.task.perform();

      task.set('complete', true);

      this.notifyPropertyChange('tasks');
    }
  }),

  actions: {
    cancel(taskIndex) {
      let tasks = this.get('tasks');
      let task = tasks.objectAt(taskIndex);

      task.set('cancelled', true);
      this.notifyPropertyChange('tasks');
    },

    cancelAll() {
      let tasks = this.get('tasks');
      tasks = A(tasks.filterBy('complete', false));
      tasks = A(tasks.filterBy('cancelled', false));

      tasks.forEach(task => task.set('cancelled', true));

      this.notifyPropertyChange('tasks');

      this.send('close');
    },

    retry() {
      let tasks = this.get('tasks');

      tasks = A(tasks.filterBy('complete', false));
      tasks = A(tasks.filterBy('cancelled', false));

      return this.get('reprocessTasks').perform(tasks);
    },

    close() {
      $('#restartableTasksModal').modal('hide');
    },

    open() {
      let tasks = A([]);

      $('#restartableTasksModal').modal('show');

      tasks.pushObject(new EmberObject({
        task: this.get('dummyTask'),
        props: {},
        description: 'Task 1',
        complete: null,
        cancelled: false,
        results: null
      }));

      for (var i = 2; i <= 10; i++) {
        tasks.pushObject(new EmberObject({
          task: this.get('dummyTask'),
          props: {},
          description: 'Task ' + i,
          complete: null,
          cancelled: false
        }));
      }

      this.set('tasks', tasks);

      return this.get('processTasks').perform(tasks);
    }
  }
});
